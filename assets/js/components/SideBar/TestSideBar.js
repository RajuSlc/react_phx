import React from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import SelectField from './SelectField';
import Button from '@material-ui/core/Button';






const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
  card: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
  },
  cardContent: {
    flexGrow: 1,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
}));

export default function TestSideBar() {
  const classes = useStyles();

  const [currency, setCurrency] = React.useState('EUR');

  const handleChange = (event) => {
    console.log("search field value", event.target.value);
  };


  return (
    <Card className={classes.card}>
      <CardContent className={classes.cardContent}>
        <form className={classes.form} noValidate autoComplete="off">
          <div>
            <SelectField />
            <SelectField />
            <SelectField />
            <SelectField />
            <SelectField />
            <SelectField />
            <SelectField />
          </div>
        </form>
      </CardContent>

    </Card>
  );
}
