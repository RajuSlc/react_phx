import React from "react";

import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import SelectField from './SelectField';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  card: {
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },

  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
}));



export default function AdCard() {
  const classes = useStyles();

  return (

    <Container className={classes.cardGrid} maxWidth="lg">
      <Card className={classes.card}>
        <CardContent className={classes.cardContent}>
          <form className={classes.form} noValidate>
          <SelectField />
          <SelectField />
          <SelectField />
          <SelectField />
          <SelectField />
          <SelectField />
          <SelectField />
            <SelectField />
          </form>
        </CardContent>
      </Card>
    </Container>




  );
}





