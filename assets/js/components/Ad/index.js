import React from 'react';
import {
  Container,
  Grid,
  makeStyles
} from '@material-ui/core';
// import Page from 'src/components/Page';
// import Profile from './Profile';
import SearchBar from '../Inventory/SearchBar';
// import AdCards from '../AdCards';
// import Scroll from '../Scroll';
import Gallery from '../Temp/Gallery';

const useStyles = makeStyles((theme) => ({
  root: {

  }
}));

const Ad = () => {
  const classes = useStyles();

  return (
      <Container className={classes.root} maxWidth="lg">
        <Grid
          container
          spacing={5}
        >          <Grid
        item
        lg={9}
        md={9}
        xs={12}
      >
        <Gallery />
      </Grid>
          <Grid
            item
            lg={3}
            md={3}
            xs={12}
          >
            <SearchBar />
          </Grid>

        </Grid>
        <Grid
          container
          spacing={5}
        >          <Grid
        item
        lg={9}
        md={9}
        xs={12}
      >
        <Gallery />
      </Grid>
          <Grid
            item
            lg={3}
            md={3}
            xs={12}
          >
            <SearchBar />
          </Grid>

        </Grid>
      </Container>

  );
};

export default Ad;
