import React from "react";

import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
});



export default function AdCard() {
  const classes = useStyles();

  return (

    <Card className={classes.card}>
    <CardMedia
      className={classes.cardMedia}
      image="https://motors.stylemixthemes.com/classified/wp-content/uploads/sites/2/2015/12/1_3-255x160.jpg"
      title="Image title"
    />
    <CardContent className={classes.cardContent}>
      <Typography gutterBottom variant="h5" component="h2">
        Heading
      </Typography>
      <Typography>
        This is a media card. You can use this section to describe the content.
      </Typography>
    </CardContent>
    <CardActions>
      <Button size="small" color="primary" href="/ad">
        View
      </Button>
      <Button size="small" color="primary" variant="contained">
        Price
      </Button>
    </CardActions>
  </Card>
  );
}





