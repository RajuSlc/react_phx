import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Gallery from './Gallery';
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: 100
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    margin: theme.spacing(1),
    width: theme.spacing(100),
    height: theme.spacing(100),
    color: theme.palette.text.secondary
  }
}));
export default function Ad() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={8}>
          <Gallery />
        </Grid>
        <Grid item xs={4}>
          <Paper className={classes.paper}>bar</Paper>
        </Grid>
      </Grid>
    </div>
  );
}