import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import CameraIcon from '@material-ui/icons/PhotoCamera';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';

import Scroll from '../Scroll';
// import FeaturedTabs from '../FeaturedTabs';
// import FTabs from '../FeaturedTabs/FTabs';
import AdCards from '../AdCards';
import Footer from '../Footer';
import { withRouter } from 'react-router-dom';
import FeaturedTabs from '../FeaturedTabs';




const useStyles = makeStyles((theme) => ({
  
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    // backgroundColor: theme.palette.background.paper,
    backgroundImage: `url(${"https://motors.stylemixthemes.com/classified/wp-content/uploads/sites/2/2016/02/listing_bnr-1917x619.jpg"})`,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
}));



const Home = (props) => {
  const classes = useStyles();
  const { history } = props;
  return (
    <React.Fragment>
      <CssBaseline />
      <main className={classes.root}>
        {/* Hero unit */}
        <div className={classes.heroContent}>
          <Container maxWidth="sm">
            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
              Album layout
            </Typography>
            <Typography variant="h5" align="center" color="textSecondary" paragraph>
              Something short and leading about the collection below—its contents, the creator, etc.
              Make it short and sweet, but not too short so folks don&apos;t simply skip over it
              entirely.
            </Typography>
            <div className={classes.heroButtons}>
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Button variant="contained" color="primary" href="/inventory/cars">
                    Buy a Car
                  </Button>
                </Grid>
                <Grid item>
                  <Button variant="contained" color="primary" href="/inventory/bikes">
                  Buy a Bike
                  </Button>
                </Grid>
              </Grid>
            </div>
          </Container>
        </div>

        <Container className={classes.cardGrid} maxWidth="lg">
          <h2 align="center"> FEATURED ITEMS</h2>
          <Divider />
          <Divider />
        </Container>
        <Container maxWidth="lg">
          <FeaturedTabs />
        </Container>
        <Divider />
        <h2 align="center"> MORE</h2>
        <Divider />
        <AdCards />
        <Scroll showBelow={250} />
      </main>
      <Footer />
    </React.Fragment>
  );
}

export default withRouter(Home);