import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import CameraIcon from '@material-ui/icons/PhotoCamera';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';

import Scroll from '../Scroll';
// import FeaturedTabs from '../FeaturedTabs';
// import FTabs from '../FeaturedTabs/FTabs';
import AdCards from '../AdCards';
// import AdCards from '../AdCards/AdCard';
import Footer from '../Footer';
import { withRouter } from 'react-router-dom';
import SideBar from '../SideBar';
import AdCard from '../AdCards/AdCard';




const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginTop: 100,
    },
    // paper: {
    //     padding: theme.spacing(1),
    //     textAlign: 'center',
    //     color: theme.palette.text.secondary,
    //     whiteSpace: 'nowrap',
    //     marginBottom: theme.spacing(1),
    // },
    sideBar: {
        justifyContent: 'center',
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    },
}));



const Inventory = (props) => {
    const classes = useStyles();
    const { history } = props;
    return (
        <React.Fragment>
            <CssBaseline />
            <main className={classes.root} >
                <Container className={classes.cardGrid} maxWidth="lg">
                    <h2 align="center"> Inventory </h2>
                    <Divider />
                    <Divider />
                    <Grid container spacing={3}>
                        <Grid className={classes.sideBar} item xs={12} sm={3}>
                            <SideBar />
                            {/* <AdCard /> */}
                        </Grid>

                        <Grid item xs={12} sm={9}>
                            <AdCards />
                            <Scroll showBelow={250} />
                        </Grid>
                    </Grid>
                </Container>
            </main>





            <Footer />
        </React.Fragment>
    );
}

export default withRouter(Inventory);