import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import { withRouter } from 'react-router-dom';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import CssBaseline from '@material-ui/core/CssBaseline';
import Link from '@material-ui/core/Link';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import FormControl from '@material-ui/core/FormControl';
import InputAdornment from '@material-ui/core/InputAdornment';



const useStyles = makeStyles((theme) => ({
    root: {
        marginBottom: 100,
    },
    inputt: {
        flexGrow: 1,
        "& > *": {
            margin: theme.spacing(1),
            width: 135,
            fontSize: 13,
            
        }
    },
    '@global': {
        ul: {
            margin: 0,
            padding: 0,
            listStyle: 'none',
        },
    },
    appBar: {
        borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbar: {
        flexWrap: 'wrap',
    },
    toolbarTitle: {
        flexGrow: 0.2,
    },
    link: {
        margin: theme.spacing(1, 1.5),
    },
    heroContent: {
        padding: theme.spacing(8, 0, 6),
    },
    cardHeader: {
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.secondary[200] : theme.palette.grey[700],
    },
    cardPricing: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'baseline',
        marginBottom: theme.spacing(2),
    },
    footer: {
        borderTop: `1px solid ${theme.palette.divider}`,
        marginTop: theme.spacing(8),
        paddingTop: theme.spacing(3),
        paddingBottom: theme.spacing(3),
        [theme.breakpoints.up('sm')]: {
            paddingTop: theme.spacing(6),
            paddingBottom: theme.spacing(6),
        },
    },
}));

const MenuNavBar = (props) => {
    const { history } = props

    const classes = useStyles();
    const [auth, setAuth] = React.useState(true);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const theme = useTheme();
    const ismobile = useMediaQuery(theme.breakpoints.down('sm'));

    const handleChange = (event) => {
        setAuth(event.target.checked);
    };

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleMenuClick = (path) => {
        history.push(path);
        setAnchorEl(null);
    };

    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar position="fixed" color="default" elevation={0} className={classes.appBar}>
                <Toolbar>
                    <Typography variant="h6" className={classes.toolbarTitle}>
                        365KARS
                    </Typography>
                    <FormControl className={classes.inputt}>
                        <Input
                            id="input-with-icon-adornment"
                            placeholder="Enter ZipCode"
                            type="search"
                            startAdornment={
                                <InputAdornment position="start">
                                    <LocationOnIcon />
                                </InputAdornment>
                            }
                        />
                    </FormControl>
                    <div>
                        {ismobile ? (
                            <>
                                <IconButton
                                    aria-label="Main Menu"
                                    aria-controls="menu-appbar"
                                    aria-haspopup="true"
                                    onClick={handleMenu}
                                    color="inherit"
                                >
                                    <MenuIcon />
                                </IconButton>
                                <Menu
                                    id="menu-appbar"
                                    anchorEl={anchorEl}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    keepMounted
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open={open}
                                    onClose={() => setAnchorEl(null)}
                                >
                                    <MenuItem onClick={() => handleMenuClick('/')}>Home</MenuItem>
                                    <MenuItem onClick={() => handleMenuClick('/inventory/all')}>Inventory</MenuItem>
                                    <MenuItem onClick={() => handleMenuClick('/profile')}>Profile</MenuItem>
                                    <MenuItem onClick={() => handleMenuClick('/pricing')}>Pricing</MenuItem>
                                    <MenuItem onClick={() => handleMenuClick('/login')}>Account</MenuItem>
                                    <MenuItem onClick={() => handleMenuClick('/postad')}>PostAd</MenuItem>
                                </Menu>
                            </>)
                            : (

                                <nav>
                                    <Link variant="button" color="textPrimary" href="/" className={classes.link}>
                                        Home
                                    </Link>
                                    <Link variant="button" color="textPrimary" href="/inventory/all" className={classes.link}>
                                        Inventory
                                    </Link>
                                    <Link variant="button" color="textPrimary" href="/profile" className={classes.link}>
                                        Profile
                                    </Link>
                                    <Link variant="button" color="textPrimary" href="/pricing" className={classes.link}>
                                        Pricing
                                    </Link>
                                    <Link variant="button" color="textPrimary" href="/login" className={classes.link}>
                                        Account
                                    </Link>
                                    <Button variant="contained" color="primary" href="/postad" className={classes.link}>
                                        PostAd
                                    </Button>
                                </nav>
                            )
                        }

                    </div>
                </Toolbar>
            </AppBar>
        </div >
    );
}


export default withRouter(MenuNavBar);