import React, { useEffect } from 'react';
import { Router } from 'react-router';
import { Route, Switch, Redirect } from 'react-router-dom';
import { render } from "react-dom";
import { Provider } from 'react-redux';
import { useDispatch, useSelector } from 'react-redux';

import { history } from './helpers/history';
import { alertActions } from './actions/alertActions';
import { PrivateRoute } from './components/PrivateRoute';
import { store } from "./helpers/store";
import Inventory from './components/Inventory';
// import { LoginPage } from '../LoginPage';
// import { RegisterPage } from '../RegisterPage';

import Login from './components/Login';
import Register from './components/Register';
import Home from './components/Home';
import { HomePage } from './components/Home/HomePage';
// import Gallery from './components/Temp/Gallery';
import MenuNavBar from './components/MenuNavBar';
import Ad from './components/Ad';
import PostAd from './components/PostAd';
import Pricing from './components/Temp/Pricing';

import { makeStyles } from '@material-ui/core/styles';
import Account from './components/Account';


const useStyles = makeStyles((theme) => ({}));
const rootElement = document.getElementById('root');

const App = () => {
    const alert = useSelector(state => state.alert);
    const dispatch = useDispatch();

    const classes = useStyles();

    useEffect(() => {
        history.listen((location, action) => {
            // clear alert on location change
            dispatch(alertActions.clear());
        });
    }, []);

    return (
        <div className={classes.container}>

            <Router history={history}>
                <MenuNavBar />
                {alert.message &&
                    <div className={`alert ${alert.type}`}>{alert.message}</div>
                }
                <Switch>
                    <Route exact path="/">
                        <Home />
                    </Route>
                    <PrivateRoute exact path="/home">
                        <HomePage />
                    </PrivateRoute>
                    <Route exact path="/login">
                        <Login />
                    </Route>
                    <Route exact path="/inventory/all">
                        <Inventory type="cars"/>
                    </Route>                    
                    <Route exact path="/inventory/cars">
                        <Inventory type="cars"/>
                    </Route>
                    <Route exact path="/inventory/bikes">
                    <Inventory type="bikes"/>
                    </Route>
                    <Route exact path="/ad">
                        <Ad />
                    </Route>
                    <Route exact path="/pricing">
                        <Pricing />
                    </Route>
                    <Route exact path="/postad">
                        <PostAd />
                    </Route>
                    <Route exact path="/profile">
                        <Account />
                    </Route>
                    <Route exact path="/register">
                        <Register />
                    </Route>
                    <Redirect from="*" to="/" />
                </Switch>
            </Router>
        </div>

    )
}


render(
    <Provider store={store}>
        <App />
    </Provider>,
    rootElement
);
