import { combineReducers } from 'redux';

import auth from './auth';
import users from './users';
import alert from './alert';

const rootReducer = combineReducers({
  auth,
  users,
  alert
});

export default rootReducer;