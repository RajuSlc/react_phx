
import { userConstants } from '../constants/userConstants';

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { loggedIn: true, user } : {};

export default function auth(state = initialState, action) {
    switch (action.type) {
        case userConstants.LOGIN_REQUEST:
            return {
                loggingIn: true,
                user: action.user
            };
        case userConstants.LOGIN_SUCCESS:
            return {
                loggedIn: true,
                user: action.user
            };
        case userConstants.LOGIN_FAILURE:
            return {};
        case userConstants.LOGOUT:
            return {};
        default:
            return state
    }
}

// const initialState = {
//   isSignedIn: false,
//   details: null,
// };

// export default function user(state = initialState, action) {

//   if (action.type === 'USER_LOGIN_SUCESS') {
//     return Object.assign({}, state, {
//       details: action.payload,
//       isSignedIn: true,
//     });
//   }
//   if (action.type === 'USER_LOGIN_FAILURE') {
//     return Object.assign({}, state, {
//       details: null,
//       isSignedIn: false,
//     });
//   }
//   return state;
// }


