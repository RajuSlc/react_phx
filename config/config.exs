# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :react_phx,
  ecto_repos: [ReactPhx.Repo]

# Configures the endpoint
config :react_phx, ReactPhxWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "iloVWHsOQT1Hjdu1ekowb/PIeZmU3GcrytgiT/wMCxuYHema1Sf/0fUhR2Dqp5nR",
  render_errors: [view: ReactPhxWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: ReactPhx.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "k7wMNPbx"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

# Configures Guardian
config :react_phx, ReactPhx.Auth.Guardian,
  issuer: "ReactPhx",
  ttl: {30, :days},
  verify_issuer: true,
  secret_key: "UJDE56CQUgHLjggE8y8aYrI0G0Gxlv0fjCiJd5BLJOWVtNfWq2BFvI0WvbCl1ccL"
