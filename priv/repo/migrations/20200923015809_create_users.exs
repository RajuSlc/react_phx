defmodule ReactPhx.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :first_name, :string
      add :last_name, :string
      add :role, :string
      add :phone_number, :string
      add :email, :string
      add :username, :string
      add :password_hash, :string
      add :with_username, :boolean, default: false, null: false

      timestamps()
    end

    create unique_index(:users, [:email])
    create unique_index(:users, [:username])
  end
end
