defmodule ReactPhxWeb.Router do
  use ReactPhxWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    # # plug CORSPlug, origin: "http://localhost:8080"
    # plug CORSPlug, origin: "*"
    plug :accepts, ["json"]
  end

  pipeline :jwt_authenticated do
    plug ReactPhx.Auth.Pipeline
  end

  scope "/", ReactPhxWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/postad", PageController, :index
    get "/profile", PageController, :index
    get "/ad", PageController, :index
    get "/pricing", PageController, :index
    get "/login", PageController, :index
    get "/home", PageController, :index
    get "/register", PageController, :index
    get "/articles", PageController, :index
    get "/inventory/:type", PageController, :index


  end

  # Other scopes may use custom stacks.
  scope "/api", ReactPhxWeb do
    pipe_through :api

    # sign_in
    post "/sign_in", UserController, :sign_in
    # log out
    delete "/user", UserController, :delete

    # get users
    get "/users", UserController, :index
    # register
    post "/register", UserController, :sign_up
  end

  # Other scopes may use custom stacks.
  scope "/api/v1", ReactPhxWeb do
    pipe_through [:api, :jwt_authenticated]

    get "/get_user", UserController, :get_user
    options "/get_user", UserController, :options
  end
end
