defmodule ReactPhxWeb.UserController do
  use ReactPhxWeb, :controller

  alias ReactPhx.Users
  alias ReactPhx.Users.User

  alias ReactPhx.Auth.Guardian
  action_fallback ReactPhxWeb.FallbackController

  def index(conn, _params) do
    users = Users.list_users()
    render(conn, "index.json", users: users)
  end

  def sign_up(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- Users.create_user(user_params),
         {:ok, token, _claims} <- Guardian.encode_and_sign(user) do
      conn
      |> put_status(:created)
      |> render("user.json", %{user: user, token: token})
    end
  end

  def sign_in(conn, %{"username" => username, "password" => password}) do
    with {:ok, user, token} <- Users.token_sign_in_with_username(username, password) do
      conn
      |> put_status(:created)
      |> render("user.json", %{user: user, token: token})
    end
  end

  def sign_in(conn, %{"email" => email, "password" => password}) do
     with {:ok, user, token} <- Users.token_sign_in_with_email(email, password) do
        conn
        |> put_status(:created)
        |> render("user.json", %{user: user, token: token})
    end
  end

  def get_user(conn, _params) do
    user = Guardian.Plug.current_resource(conn)
    conn |> render("user.json", %{user: user})
  end

  def delete(conn, _params) do
    jwt = Guardian.Plug.current_token(conn)
    Guardian.revoke(jwt)
    conn
    |> put_status(:ok)
  end


end
