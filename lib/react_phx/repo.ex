defmodule ReactPhx.Repo do
  use Ecto.Repo,
    otp_app: :react_phx,
    adapter: Ecto.Adapters.Postgres
end
