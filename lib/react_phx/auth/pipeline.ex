defmodule ReactPhx.Auth.Pipeline do
  use Guardian.Plug.Pipeline, otp_app: :ReactPhx,
  module: ReactPhx.Auth.Guardian,
  error_handler: ReactPhx.Auth.ErrorHandler

  plug Guardian.Plug.VerifyHeader, realm: "Bearer"
  plug Guardian.Plug.EnsureAuthenticated
  plug Guardian.Plug.LoadResource
end
