defmodule ReactPhx.Users.User do
  use Ecto.Schema
  import Ecto.Changeset
  import Comeonin.Bcrypt, only: [hashpwsalt: 1]
  schema "users" do
    field :email, :string
    field :first_name, :string
    field :last_name, :string
    field :password_hash, :string
    field :phone_number, :string, default: ""
    field :role, :string, default: "user"
    field :username, :string, default: ""
    field :with_username, :boolean, default: false

    # Virtual fields
    field :password, :string, virtual: true

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:first_name, :last_name, :role, :phone_number, :email, :username, :password, :with_username])
    |> validate_required([:first_name, :last_name, :role, :phone_number, :email, :username, :password, :with_username])
    |> validate_format(:email, ~r/@/) # Check that email is valid
    |> validate_length(:password, min: 6) # Check that password length is >= 6
    |> unique_constraint(:email)
    |> unique_constraint(:username)
    |> validate_phone(:phone_number)
    |> validate_username(:username)
    |> put_password_hash # Add put_password_hash to changeset pipeline
  end

  defp validate_phone(changeset, field) do
    phone = get_field(changeset, field)
    case phone do
      "" ->
        add_error(changeset, field, "Please enter Phone Number")
      phone ->
        if String.length(phone) != 10 do
          add_error(changeset, field, "Invalid Phone number")
        else
          changeset
        end
    end
  end


  defp validate_username(changeset, field) do
    username = get_field(changeset, field)
    case username do
      "" ->
        add_error(changeset, field, "Please enter a User Name")
      username ->
        if String.length(username) <= 6 do
          add_error(changeset, field, "username length should be more than 6")
        else
          changeset
        end
    end
  end


  defp put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}}
        ->
          put_change(changeset, :password_hash, hashpwsalt(pass))
      _ ->
          changeset
    end

  end
end
